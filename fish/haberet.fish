function haberet
	set CMDSTART (date +"%T")
    fish -c "$argv"
    set CMDEND (date +"%T")
    and notify-send "finished: $argv" "start: $CMDSTART
end: $CMDEND"
    mpv /usr/share/sounds/freedesktop/stereo/alarm-clock-elapsed.oga >/dev/null 2>&1 &
    xcowsay "Finished: $argv"
    #ping -c1 google.com
    #if test $status = 0
    #    curl --data "apikey=<APIKEY>&application=Cron&event=$argv[1] finished&description=CmdNotify" https://www.notifymyandroid.com/publicapi/notify >/dev/null 2>&1 &
    #end
end
