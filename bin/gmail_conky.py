#!/usr/bin/env python2
# get unread email list from gmail and format output for conky
from xml.dom import minidom

import urllib2, base64

username = 'yourusername@gmail.com'
password = 'yourpassword'
url='https://mail.google.com/mail/feed/atom'
request = urllib2.Request(url)
base64string = base64.b64encode('%s:%s' % (username, password))
request.add_header("Authorization", "Basic %s" % base64string)
result = urllib2.urlopen(request)

xmldoc = minidom.parse(result)

def getText(nodelist):
    rc = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)

emaillist = xmldoc.getElementsByTagName('entry')
if len(emaillist) > 0:
    print('${voffset 6}${color}${font StyleBats:size=17}v${font Terminus:size=12} EMAILS ${font}${hr 2}')
    print("${color slate grey}You have ${color green}" + str(len(emaillist)) + "${color slate grey} new emails${color}")
    ecount=0
    for entry in emaillist:
        if ecount == 5: # list only 5
            break
        count=0
        fromlist = entry.getElementsByTagName('email')
        subjectlist = entry.getElementsByTagName('title')
        print("%s" % "${color slate grey}From: ${color green}" + getText(fromlist[count].childNodes) + "${color slate grey}")
        print('%s' % getText(subjectlist[count].childNodes).encode('utf-8').strip())
        count += 1
        ecount += 1
