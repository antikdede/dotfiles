#!/bin/bash
# get tasks list from nextcloud
ncUsername='<USERNAME>'
ncPassword='<PASSWORD>'
ncHostPort='<IP|HOST:PORT>'
TASKS=$(curl -X REPORT http://${ncUsername}:${ncPassword}@${ncHostPort}/remote.php/dav/calendars/${ncUsername}/personal/ -H "Depth: 1" -d '<c:calendar-query xmlns:c="urn:ietf:params:xml:ns:caldav" xmlns:d="DAV:" xmlns:a="http://apple.com/ns/ical/" xmlns:o="http://owncloud.org/ns"><d:prop><d:getetag/><c:calendar-data/></d:prop><c:filter><c:comp-filter name="VCALENDAR"><c:comp-filter name="VTODO"><c:prop-filter name="COMPLETED"><c:is-not-defined/></c:prop-filter></c:comp-filter></c:comp-filter></c:filter></c:calendar-query>' | grep SUMMARY | cut -d ':' -f 2)
echo '${voffset 4}${color}${font StyleBats:size=17}v${font Terminus:size=12} TASKS ${font}${hr 2}${color}'
echo "\${color green}$TASKS\${color}"
