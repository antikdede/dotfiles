#!/bin/bash
cd $HOME/Music/
function bulcal () {
ls | sort -R |tail -n 1 -$N | while read file; do
  if [[ -d "$file" ]]; then
      cd "$file"
      bulcal
  elif [[ -f "$file" ]] && [ ${file: -4} == ".mp3" ] ; then
      echo "will play $file"
      mpv "$file"
  else
      bulcal
  fi
done
}
bulcal
