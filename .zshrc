# path to antigen script
source ~/.zsh/antigen.zsh
antigen use oh-my-zsh
antigen-bundles <<EOF
git
command-not-found
pip
web-search
virtualenvwrapper
pass
colored-man-pages
colorize
extract
systemd
z
history-substring-search
tmuxinator
zsh-users/zsh-autosuggestions
zsh-users/zsh-syntax-highlighting
EOF
antigen theme ~/.zsh/ fino-time
#antigen theme xiong-chiamiov-plus
antigen apply
export LANG=en_US.UTF-8
export BYOBU_CONFIG_DIR=$HOME/.byobu
source ~/.zsh/aliases.zsh
source ~/.zsh/functions.zsh
unsetopt correct
