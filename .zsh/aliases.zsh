#alias vi="vim"
alias rmorphans="sudo pacman -Rns $(pacman -Qtdq)"
alias varlog="journalctl -xe $@"
alias taillog="journalctl -xef $@"
alias ezsh='vim ~/.zshrc && source ~/.zshrc'
alias ezvim='vim ~/.vimrc'
alias lsat="ls -lat"
alias lstoday="ls  -tal --time-style='+%D %H' | grep `date +%D`"
alias woltv="wol <MACADDR>"
alias top10='print -l ${(o)history%% *} | uniq -c | sort -nr | head -n 10'
alias topten="history | awk '{CMD[\$2]++;count++;}END { for (a in CMD)print CMD[a] \" \" CMD[a]/count*100 \"% \" a;}' | grep -v \"./\" | column -c3 -s \" \" -t | sort -nr | nl |  head -n10"
