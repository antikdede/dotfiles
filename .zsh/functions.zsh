function paclog () {
       if (( $# == 0 ))
       then cat /var/log/pacman.log | grep -i installed;
       else
       cat /var/log/pacman.log | grep -i $1
       fi
 }

function haberet () {
	CMDSTART=`date +"%T"`
  CMD="$@"
    eval $CMD
    CMDEND=`date +"%T"`
    notify-send "finished: $CMD" "start: $CMDSTART
end: $CMDEND"
    (play -q /usr/share/sounds/freedesktop/stereo/alarm-clock-elapsed.oga) &
    xcowsay "Finished: $CMD"
    #telegram-cli -k opt/Telegram/tg-server.pub -WR -e "msg @ertan Finished: $CMD"
}
function kbem () {
    echo -n 'Enter message: '
    read msg && export kbmsg=$msg
    echo -n "will send $kbmsg\nEnter recipient: " 
    read rcpt && export kbrcpt=$rcpt
    echo "will send $kbmsg to $kbrcpt"
    keybase encrypt -m "$kbmsg" "$kbrcpt" > /keybase/private/ertan,$kbrcpt/`date +%d%m%Y-%H%M%S`.txt
}

function megale () {
    LINK=$(megals -e $1 | cut -d ' ' -f 4);
    echo $LINK
}

function gitpush () {
	CURDIR=$PWD
    echo "pushing dotfiles repo"
	cd ~/.dotfiles && git add . && git commit -m "otokomit" && git push
    echo "pushing password-store repo"
	pass git push
    echo "syncing tasks"
	task sync
	cd $CURDIR
}

function gitpull () {
	CURDIR=$PWD
    echo "pulling dotfiles repo"
	cd ~/.dotfiles && git pull
    echo "pulling password-store repo"
	pass git pull
    echo "syncing tasks"
	task sync
	cd $CURDIR
}

function paci () {
    find $HOME/.cache/pacaur/ -mindepth 1 -exec rm -fr "{}" \; -print
    pacaur -S --noedit --needed "$@"
}

function pacu () {
    find $HOME/.cache/pacaur/ -mindepth 1 -exec rm -fr "{}" \; -print
    pacaur -Syu --noedit
    sudo paccache -rk 2
}

function pacr () {
    sudo pacman -Rs "$@"
}

function pacas () {
    pacaur -Ss -s "$@"
}

function pacinfo () {
    pacaur -Si "$@"
}
function urldecode () {
    if [ $@ ] ; then
        echo "$@" | awk -niord '{printf RT?$0chr("0x"substr(RT,2)):$0}' RS=%..
    else
        awk -niord '{printf RT?$0chr("0x"substr(RT,2)):$0}' RS=%..
    fi
}
